package com.karthik;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Pattern;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Environment;
import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ImageView;

public class MainActivity extends Activity implements SensorEventListener {
	// double sample[] = new double[1000];
	// int location[] = new int[1000];
	ImageView image;
	List<Double> sampleList = new ArrayList<Double>();
	List<Integer> locationList = new ArrayList<Integer>();
	String xmlFileName = "fingerPrint.xml";
	private  int i = 0;
	private  int locCnt = 0;
	private  int pos = 0;
	int prevStepCount = 0;
	boolean isStart = false;
	final double STRIDE_LENGTH = 2.5;// Not used.. Need to calculate dynamically
	final int NUM_STEPS = 4;

	Map<Integer, Integer> distanceLocation = new HashMap<Integer, Integer>();
	List<FingerPrint> rssiValues = new ArrayList<FingerPrint>();
	Map<Integer, List<String>> testingRSSI = new HashMap<Integer, List<String>>();
	List<SFFP> sffpCorridorfList = new ArrayList<SFFP>();
	List<SFFP> sffpRoomfList = new ArrayList<SFFP>();
	List<SFFP> sffprefPts = new ArrayList<SFFP>();
	List<FingerPrint> fingerpoints;
	private SensorManager sm;
	MDS mds = new MDS();
	FingerPrintSpace fingerPrintSpace = new FingerPrintSpace();
	StepCount stepCountObj = new StepCount();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Context context = getApplicationContext();

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		sm = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
		// List<Sensor> sensorList =
		// sm.getSensorList(Sensor.TYPE_ACCELEROMETER);

		sm.registerListener(this,
				sm.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
				SensorManager.SENSOR_DELAY_NORMAL);
		List<Integer> fromPos = new ArrayList<Integer>();
		fromPos.add(0);
		FingerPrint fingerPrint = new FingerPrint(0, getRSSI(), pos++, fromPos);
		rssiValues.add(fingerPrint);
		// testingRSSI.put(locCnt, getRSSI1());
		distanceLocation.put(locCnt, 0);
		locationList.add(0);
		locCnt++;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	public void btnlocate_OnClick(View view) {

		/*
		 * Intent i; PackageManager manager = getPackageManager(); try { i =
		 * manager.getLaunchIntentForPackage("net.cloudpath.xpressconnect"); if
		 * (i == null) throw new PackageManager.NameNotFoundException();
		 * i.addCategory(Intent.CATEGORY_LAUNCHER); startActivity(i); } catch
		 * (PackageManager.NameNotFoundException e) {
		 * 
		 * }
		 */
		isStart = true;
		
		try {
			sffpCorridorfList = loadSFFP("SFFP_corridor.txt");
			Log.d("SFFP_corridor", "Corridor completed");
			sffpRoomfList=loadSFFP("SFFP_room.txt");
			sffprefPts = extractRefPoints(sffpCorridorfList);
			FileWrite.writeToFile(null, "Completed loading", "Tracker.txt", true);
			File root = Environment.getExternalStorageDirectory();
			File file = new File(root, xmlFileName);
			if(file.exists() && file.canRead())
			{
				JAXB jaxb = new JAXB();
				fingerpoints = jaxb.getFingerPrint(xmlFileName);
				
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

	}

	public void Query_OnClick(View view) throws IOException {

		/*
		 * int stepCount = stepCountObj.calculateStepCount(i, sampleList);
		 * TextView txtview = (TextView) findViewById(R.id.textView3);
		 * txtview.setText("#Steps: " + stepCount);
		 */
		String mess = locationQuery(fingerpoints).toLowerCase(Locale.ENGLISH);
		image=(ImageView)findViewById(R.id.imageView1); 
		//FileWrite.writeToFile(null,image.getHeight() + "-", "UIError", true);
		try {
			if (mess.contains("projector")) {
				image.setImageResource(R.drawable.projector);
			}
			if (mess.contains("corridor")) {

				image.setImageResource(R.drawable.corridor);
			}
			if (mess.contains("middle")) {

				image.setImageResource(R.drawable.middle);
			}
			if (mess.contains("board")) {
				image.setImageResource(R.drawable.board);

			}
			if (mess.contains("bottom")) {

				image.setImageResource(R.drawable.bottom);
			}
			if (mess.contains("door")) {
				image.setImageResource(R.drawable.door);

			}
		}

		catch (Exception e) {
			FileWrite.writeToFile(null, e.getMessage() + e.getLocalizedMessage() + e.toString(), "UIError.txt", true);

		}

	}

	public void SFFP_OnClick(View view) throws IOException {
		double d[][] = new double[locCnt][locCnt];

		for (int x = 0; x < locCnt; x++) {
			for (int y = 0; y < locCnt; y++) {
				d[x][y] = (x == y ? 0 : Math.abs(locationList.get(y) - locationList.get(x)));
			}
		}
		FileWrite.writeToFile(d, "", "logdij.txt", false);
		double X[][] = mds.CreateMDS(d);
		FileWrite.writeToFile(X, "", "SFFP.txt", false);
	}
	public List<SFFP> loadSFFP(String fileName) throws IOException
	{
		File root = Environment.getExternalStorageDirectory();
		File file = new File(root, fileName);
		Pattern p = Pattern.compile("\\|");
		List<SFFP> sffpfList = new ArrayList<SFFP>();
		String line =null;
		if(root.canRead()){
			BufferedReader br = new BufferedReader(new FileReader(file));
			while((line =br.readLine()) !=null)
			{
				SFFP sffp = new SFFP();
				String splitted[]= p.split(line);
				
				if(splitted.length > 0)
				{
					try {
						//Log.d(fileName, splitted[0]);
					sffp.setX(Double.parseDouble(splitted[0]));
					
					sffp.setY(Double.parseDouble(splitted[1]));
					sffp.setRoomNum(Integer.parseInt(splitted[2]));
					sffp.setLocationName(splitted[3]);
					if(fileName.contains("corridor"))
					{
						sffp.setNearRoomNum(Integer.parseInt(splitted[4]));
					}
					}
					catch(NumberFormatException e){
						Log.d(fileName + "Exception", e.getMessage());
					}
				}
				sffpfList.add(sffp);
				
			}
			br.close();
		}
		return sffpfList;
	}
	
	public List<SFFP> extractRefPoints(List<SFFP> corridorList)
	{
		List<SFFP> refpoints = new ArrayList<SFFP>();
		
		for(SFFP sffp: corridorList)
		{
			if(sffp.getNearRoomNum()!=0)
			{
				refpoints.add(sffp);
			}
		}
		Log.d("CorridorList" , "" + refpoints.size());
		return refpoints;
	}

	public void FingerPrint_OnClick(View view) throws IOException {

		File root = Environment.getExternalStorageDirectory();
		File file = new File(root, "rssiValues.txt");

		if (root.canWrite()) {
			FileWriter filewriter = new FileWriter(file, false);
			for (FingerPrint fp : rssiValues) {
				
				StringBuilder sb = new StringBuilder();
				sb.append(fp.getPos() + " " + fp.getFromPos() + " " + fp.getDistance()  + "\n");
				filewriter.write(sb.toString());
			}
			/*
			 * for (FingerPrint fingerprint : rssiValues) {
			 * 
			 * StringBuilder sb = new StringBuilder(); sb.append("# " +
			 * fingerprint.getPos() + " From: " + fingerprint.getFromPos() +
			 * " Distance: " + fingerprint.getDistance() + "\n");
			 * 
			 * filewriter.write(sb.toString()); }
			 */

			filewriter.close();
		}
		
		fingerpoints=fingerPrintSpace.findDifference(rssiValues);
		double d[][] = fingerPrintSpace.createFingerPrintSpace(fingerpoints);
		FileWrite.writeToFile(d, "", "d.txt", false);
		double X[][] = null;
		if (d.length > 0) {
			 X= mds.CreateMDS(d);
			FileWrite.writeToFile(X, "", "FingerPrintSpaceMDS.txt",false);
		} else
			FileWrite.writeToFile(d, "size:" + d.length, "FingerPrintSpaceMDS.txt",false);
		
		if(X!=null && X.length == fingerpoints.size()){
		for(int cnt =0;cnt < X.length;cnt++){
		FingerPrint fingerPrint = fingerpoints.get(cnt);
		fingerPrint.setX(X[cnt][0]);
		fingerPrint.setY(X[cnt][1]);
		
		}
		}
		Mapping mapping = new Mapping();
		fingerpoints = mapping.getBetweeness(fingerpoints);
	
		
		File file1 = new File(root, "betweeness.txt");
		if (root.canWrite()) {
			FileWriter filewriter = new FileWriter(file1, false);
			for (FingerPrint fp : fingerpoints) {
				StringBuilder sb = new StringBuilder();
				sb.append(fp.getPos() + " :  " + fp.getBetweeness()  + "\n");
				filewriter.write(sb.toString());
			}
			filewriter.close();
		}
		
		//Filter the finger prints based on the watershed
		fingerpoints=mapping.getCorridorPoints(fingerpoints);
		
		File mdsFile = new File(root,"Roompoints.txt");
		if (root.canWrite()) {
			FileWriter filewriter = new FileWriter(mdsFile, false);
			for (FingerPrint fp : fingerpoints) {
				if(!fp.isCorridor()){
				StringBuilder sb = new StringBuilder();
				sb.append(fp.getX() + " " + fp.getY()  + "\n");
				filewriter.write(sb.toString());
				}
			}
			filewriter.close();
		}
		List<Cluster> cluster = mapping.getClusters(mdsFile);
	
		for(FingerPrint fingerPrint: fingerpoints){
			for(Cluster cl : cluster)
			{
				if(fingerPrint.getX() == cl.getX() && fingerPrint.getY() == cl.getY())
				{
					fingerPrint.setRoomPos(cl.getRoomPos());
					break;
				}
					
			}
		}
		
		File clusterFile = new File(root,"Clustering.txt");
		if (root.canWrite()) {
			FileWriter filewriter = new FileWriter(clusterFile, false);
			for (FingerPrint fp : fingerpoints) {
				if(!fp.isCorridor()){
				StringBuilder sb = new StringBuilder();
				sb.append(fp.getPos() + " " + fp.getRoomPos()  + "\n");
				filewriter.write(sb.toString());
				}
			}
			filewriter.close();
		}
		
		fingerpoints = mapping.refPointMapping(fingerpoints, cluster, sffprefPts, sffpRoomfList);
		fingerpoints = mapping.mergeSFFPFingerPrint(fingerpoints, sffpRoomfList, sffpCorridorfList);
		
		File finalDatabase = new File(root,"finalDatabase.txt");
		if (root.canWrite()) {
			FileWriter filewriter = new FileWriter(finalDatabase, false);
			for (FingerPrint fp : fingerpoints) {
				StringBuilder sb = new StringBuilder();
				sb.append(fp.getPos() + " " + fp.getRealroomPos() + " " + fp.getLocationName() +  "\n");
				filewriter.write(sb.toString());
			}
			filewriter.close();
		}
		isStart=false;
		
		JAXB jaxb = new JAXB();
		jaxb.writeToXML(fingerpoints, xmlFileName);
		/*List<FingerPrint> fingerpoints;
		fingerpoints=fingerPrintSpace.findDifference(rssiValues);
		double d[][] = fingerPrintSpace.createFingerPrintSpace(fingerpoints);
		FileWrite.writeToFile(d, "", "d.txt", false);
		LoadGraph loadGraph = new LoadGraph();
		Graph graph= loadGraph.populateGraph(fingerpoints, d);
		FloydWarshallAPSP floydWarshall = new FloydWarshallAPSP();
		double adj[][] = floydWarshall.getShortestPath(graph);
		FileWrite.writeToFile(adj, "", "Shortestpath.txt", false);*/
		
	}
	
	

	@Override
	public void onAccuracyChanged(Sensor arg0, int arg1) {
		// TextView txtview = (TextView) findViewById(R.id.textView1);
		switch (arg1) {
		case SensorManager.SENSOR_STATUS_ACCURACY_HIGH:
			// txtview.setText("SENSOR_STATUS_ACCURACY_HIGH");
			break;
		case SensorManager.SENSOR_STATUS_ACCURACY_MEDIUM:
			// txtview.setText("SENSOR_STATUS_ACCURACY_MEDIUM");
			break;
		case SensorManager.SENSOR_STATUS_ACCURACY_LOW:
			// txtview.setText("SENSOR_STATUS_ACCURACY_LOW");
			break;
		case SensorManager.SENSOR_STATUS_UNRELIABLE:
			// txtview.setText("SENSOR_STATUS_UNRELIABLE");
			break;
		}

	}

	@Override
	protected void onResume() {
		super.onResume();
		sm.registerListener(this,
				sm.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
				SensorManager.SENSOR_DELAY_NORMAL);
	}

	@Override
	protected void onPause() {
		sm.unregisterListener(this);
		super.onPause();
	}

	public String locationQuery(List<FingerPrint> fingerPoints) {
		List<RSSI> locationFingerPrint = getRSSI();
		Collections.sort(locationFingerPrint);
		FingerPrintSpace fpSpace = new FingerPrintSpace();
		double sum=0;
		double closest;
		String locationName ="";
		closest = Double.POSITIVE_INFINITY;
		for(FingerPrint fingerPrint: fingerPoints)
		{
			sum = fpSpace.getDiff(locationFingerPrint, fingerPrint.getFingerprints());
			if(sum < closest)
			{
				locationName = fingerPrint.getLocationName();
				closest =sum;
			}
		}
		Context context = getApplicationContext();;
		String mess = "Your are in " + locationName;
		//Toast.makeText(context, "SUM: " + sum, Toast.LENGTH_LONG).show();
		Toast.makeText(context, mess, Toast.LENGTH_LONG).show();
		return mess;
	}
	@Override
	public void onSensorChanged(SensorEvent event) {
		onAccuracyChanged(event.sensor, event.accuracy);
		//TextView txtview2 = (TextView) findViewById(R.id.textView2);
		TextView txtview3 = (TextView) findViewById(R.id.textView3);
		TextView txtview1 = (TextView) findViewById(R.id.textView1);
		float x = 0;
		float y = 0;
		float z = 0;
		switch (event.sensor.getType()) {
		case Sensor.TYPE_ACCELEROMETER:
			x = event.values[0];
			y = event.values[1];
			z = event.values[2];
			break;
		}
		int stepCount = 0;
		if (isStart) {

			double magnitude = Math.sqrt(x * x + y * y + z * z);
			//if (i < 1000)
			
				//sample[i++] = magnitude;
				sampleList.add(magnitude);
				i++;
			
			// testingRSSI.put(locCnt, getRSSI1());
			// if (locCnt < 20) {

			txtview1.setText("#Location: " + locCnt);
			stepCount = stepCountObj.calculateStepCount(i, sampleList);
			txtview3.setText("# Steps Walked: " + stepCount);
			if (stepCount >= NUM_STEPS && stepCount % NUM_STEPS == 0
					&& prevStepCount != stepCount) {
				List<Integer> fromPos = new ArrayList<Integer>();
				fromPos.add(pos - 1);
				FingerPrint fingerPrint = new FingerPrint(NUM_STEPS, getRSSI(),
						pos, fromPos);
				pos++;
				rssiValues.add(fingerPrint);

				distanceLocation.put(locCnt, stepCount);
				//location[locCnt++] = stepCount;
				locationList.add(stepCount);
				locCnt++;
				prevStepCount = stepCount;
			}
		}
		// }
	}

	public List<RSSI> getRSSI() {
		WifiManager wifi = (WifiManager) getSystemService(WIFI_SERVICE);
		wifi.startScan();
		List<ScanResult> wifiResult = wifi.getScanResults();

		int size = wifiResult.size();
		List<RSSI> rssi = new ArrayList<RSSI>();

		for (int cnt = 0; cnt < size; cnt++) {

			ScanResult scanResult = wifiResult.get(cnt);
			RSSI rSSi = new RSSI(scanResult.SSID,scanResult.BSSID,
					scanResult.level);
			rssi.add(rSSi);
		}

		return rssi;
	}

	public List<String> getRSSI_test() {
		WifiManager wifi = (WifiManager) getSystemService(WIFI_SERVICE);
		wifi.startScan();
		List<ScanResult> wifiResult = wifi.getScanResults();

		int size = wifiResult.size();
		List<String> rssi = new ArrayList<String>();

		for (int i = 0; i < size; i++) {
			ScanResult scanResult = wifiResult.get(i);
			String SSID = scanResult.SSID;
			String bSSID = scanResult.BSSID;
			String rssiString0 = String.valueOf(scanResult.level);

			rssi.add(SSID + "," + bSSID + "," + rssiString0);
		}
		return rssi;
	}

}
