package com.karthik;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import android.os.Environment;

public class FileWrite {
	public static void writeToFile(double d[][], String valueToWrite, String fileName,boolean append)
			throws IOException {

		
		if (valueToWrite.equals("") && d.length != 0) {
			int m = d.length;
			int n = d[0].length;
			//valueToWrite = m + "\n";
			for (int i = 0; i < m; i++) {
				for (int j = 0; j < n; j++) {
					valueToWrite = valueToWrite + d[i][j] + " ";
				}
				valueToWrite += "\n";
			}
		}

		File root = Environment.getExternalStorageDirectory();
		File file = new File(root, fileName);

		if (root.canWrite()) {
			FileWriter filewriter = new FileWriter(file, append);
			filewriter.write(valueToWrite);
			filewriter.close();
		}
	}

}
