package com.karthik;

import org.simpleframework.xml.Element;

public class RSSI implements Comparable<RSSI>{
	@Element
	private String SSID;
	@Element
	private String BSSID;
	@Element
	private int rssiLevel;

	public RSSI(@Element(name = "SSID") String sSID,
			@Element(name = "BSSID") String bSSID,
			@Element(name = "rssiLevel") int rssiLevel) {
		this.setBSSID(bSSID);
		this.setRssiLevel(rssiLevel);
		this.setSSID(sSID);

	}

	public String getSSID() {
		return SSID;
	}

	public void setSSID(String sSID) {
		SSID = sSID;
	}

	public String getBSSID() {
		return BSSID;
	}

	public void setBSSID(String bSSID) {
		BSSID = bSSID;
	}

	public int getRssiLevel() {
		return rssiLevel;
	}

	public void setRssiLevel(int rssiLevel) {
		this.rssiLevel = rssiLevel;
	}

	@Override
	public int compareTo(RSSI y) {
		return this.getBSSID().compareTo(y.getBSSID());
	}

}
