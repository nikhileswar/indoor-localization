package com.karthik;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import net.sf.javaml.clustering.Clusterer;
import net.sf.javaml.clustering.KMeans;
import net.sf.javaml.core.Dataset;
import net.sf.javaml.core.Instance;
import net.sf.javaml.tools.data.FileHandler;

import org.graphstream.algorithm.BetweennessCentrality;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.SingleGraph;

public class Mapping {
	final double WATERSHED = 900;
	final int CLUSTER_SIZE = 3;
	final double CORRIDOR_RATIO = 0.2;
	/*library(scatterplot3d)
	attach(mtcars)
	scatterplot3d(wt,disp,mpg, main="3D Scatterplot")*/
	public List<FingerPrint> getBetweeness(List<FingerPrint> fingerpoints) {
		Graph graph1 = new SingleGraph("Betweeness");
		List<Node> nodes = new ArrayList<Node>();
		BetweennessCentrality bcb = new BetweennessCentrality();
		bcb.setWeightAttributeName("weight");

		for (int i = 0; i < fingerpoints.size(); i++) {
			FingerPrint rssiX = fingerpoints.get(i);
			if (rssiX.getPos() != 0) {
				for (int fromPos : rssiX.getFromPos()) {
					Node x, y;
					if ((x = graph1.getNode(fromPos + "")) == null)
						x = graph1.addNode(fromPos + "");
					if ((y = graph1.getNode(rssiX.getPos() + "")) == null)
						y = graph1.addNode(rssiX.getPos() + "");
					if (!nodes.contains(x))
						nodes.add(x);
					if (!nodes.contains(y))
						nodes.add(y);
					graph1.addEdge(fromPos + "" + rssiX.getPos(), fromPos + "",
							rssiX.getPos() + "");
					bcb.setWeight(x, y, rssiX.getDistance());

				}

			}
		}

		bcb.init(graph1);
		bcb.compute();

		for (int i = 0; i < nodes.size(); i++) {
			Node node = nodes.get(i);

			for (FingerPrint fingerPrint : fingerpoints) {
				if (fingerPrint.getPos() == Integer.parseInt(node.getId())) {
					fingerPrint.setBetweeness((Double) node.getAttribute("Cb"));
					break;
				}
			}
		}

		return fingerpoints;

	}

	public List<FingerPrint> getCorridorPoints(List<FingerPrint> fingerpoints) {

		int numCorridor = (int) Math.floor(fingerpoints.size() * CORRIDOR_RATIO);
		int midPoint = (int) Math.ceil(fingerpoints.size() / 2);
		int start = midPoint - numCorridor / 2;
		int end = midPoint + numCorridor / 2;
		for (int i = 0; i < fingerpoints.size(); i++) {
			FingerPrint fingerPrint = fingerpoints.get(i);
			if ((i >= start && i <= end) || fingerPrint.getBetweeness() == 0)
				fingerPrint.setCorridor(true);
			else
				fingerPrint.setCorridor(false);
		}
		

		/*for (FingerPrint fingerPrint : fingerpoints) {
			if (fingerPrint.getBetweeness() > WATERSHED
					|| fingerPrint.getBetweeness() == 0)
				fingerPrint.setCorridor(true);
			else
				fingerPrint.setCorridor(false);
		}*/
		return fingerpoints;
	}

	public List<Cluster> getClusters(File mdsFile) throws IOException {
		Dataset data = FileHandler.loadDataset(mdsFile, " ");
		Clusterer km = new KMeans(CLUSTER_SIZE);
		Dataset[] clusters = km.cluster(data);
		FileWrite.writeToFile(null, "Cluster Length: " + clusters.length,
				"ClusterLength.txt", false);
		List<Cluster> clusterMap = new ArrayList<Cluster>();

		int j = 0;
		for (Dataset ds : clusters) {
			++j;
			for (Instance ins : ds) {
				Cluster cluster = new Cluster();
				for (Map.Entry<Integer, Double> entry : ins.entrySet()) {
					if (entry.getKey() == 0)
						cluster.setX(entry.getValue());
					else
						cluster.setY(entry.getValue());

				}
				cluster.setRoomPos(j);
				clusterMap.add(cluster);

			}
		}
		return clusterMap;
	}

	public List<FingerPrint> refPointMapping(List<FingerPrint> fingerpoints,
			List<Cluster> clusters, List<SFFP> refsffp, List<SFFP> roomsffp) {
		double temp;
		double closest;
		int clusterPos;
		FingerPrint closestPrint;
		List<FingerPrint> referencePoints = new ArrayList<FingerPrint>();
		for (int i = 1; i <= CLUSTER_SIZE; i++) {
			closest = Double.POSITIVE_INFINITY;
			closestPrint = null;
			clusterPos = 0;
			for (FingerPrint fingerPrint : fingerpoints) {
				if (fingerPrint.isCorridor()) {
					for (Cluster cluster : clusters) {
						if (cluster.getRoomPos() == i) {
							temp = getDistance(fingerPrint, cluster);
							if (temp < closest) {
								closest = temp;
								closestPrint = fingerPrint;
								clusterPos = i;
							}
						}
					}
				}
			}
			for (FingerPrint fingerPrint : fingerpoints) {
				if (!fingerPrint.isCorridor()) {
					if (fingerPrint.getRoomPos() == clusterPos) {
						fingerPrint.setRefptPos(closestPrint.getPos());
					}
				}
			}
			if (closestPrint != null)
				referencePoints.add(closestPrint);

			StringBuilder sb = new StringBuilder();
			for (FingerPrint fp : referencePoints) {

				sb.append(fp.getPos() + "  : " + fp.getX() + "  : " + fp.getY()
						+ "\n");
			}
			try {
				FileWrite.writeToFile(null, sb.toString(), "refPoints1.txt",
						false);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		try {
			FileWrite.writeToFile(null,
					"Completed calculating reference points", "Tracker.txt",
					true);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		List<FingerPrint> refPts = compareSFFPFingerPrint(referencePoints,
				refsffp);
		StringBuilder sb = new StringBuilder();
		for (FingerPrint fp : refPts) {

			sb.append(fp.getPos() + "  : " + fp.getRealroomPos() + "\n");
		}
		try {
			FileWrite.writeToFile(null, "Got reference points", "Tracker.txt",
					true);
			FileWrite.writeToFile(null, sb.toString(), "refPoints2.txt", false);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		for (FingerPrint refPt : refPts) {
			for (FingerPrint fingerPrint : fingerpoints) {
				if (!fingerPrint.isCorridor()
						&& fingerPrint.getRefptPos() == refPt.getPos()) {
					fingerPrint.setRealroomPos(refPt.getRealroomPos());

				}
			}
		}
		StringBuilder sb1 = new StringBuilder();
		for (FingerPrint fingerPrint : fingerpoints) {
			if (!fingerPrint.isCorridor()) {
				sb1.append(fingerPrint.getPos() + " : "
						+ fingerPrint.getRefptPos() + " : "
						+ fingerPrint.getRoomPos() + " : "
						+ fingerPrint.getRealroomPos() + "\n");
			}
		}
		try {

			FileWrite
					.writeToFile(null, sb1.toString(), "refPoints3.txt", false);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return fingerpoints;
	}

	public List<FingerPrint> mergeSFFPFingerPrint(
			List<FingerPrint> fingerpoints, List<SFFP> roomsffp,
			List<SFFP> corffp) {
		double temp;
		double closest;
		double temp1;
		double closest1;

		for (FingerPrint fingerPrint : fingerpoints) {
			closest = Double.POSITIVE_INFINITY;
			closest1 = Double.POSITIVE_INFINITY;
			if (!fingerPrint.isCorridor()) {
				for (SFFP sp : roomsffp) {
					if (sp.getRoomNum() == fingerPrint.getRealroomPos()) {
						temp = getDistance(fingerPrint, sp);
						if (temp < closest) {
							closest = temp;
							fingerPrint.setLocationName(sp.getLocationName());
						}
					}
				}
			} else {
				for (SFFP sp : corffp) {
					/*
					 * if (sp.getNearRoomNum() == fingerPrint.getRealroomPos())
					 * { fingerPrint.setLocationName(sp.getLocationName()); flag
					 * =true; break; } if(!flag) {
					 */

					if (fingerPrint.getRealroomPos() != 0
							&& sp.getNearRoomNum() == fingerPrint
									.getRealroomPos()) {
						fingerPrint.setLocationName(sp.getLocationName());
						break;
					}
					temp1 = getDistance(fingerPrint, sp);
					if (temp1 < closest1) {
						closest1 = temp1;
						fingerPrint.setLocationName(sp.getLocationName());
					}
					// }
				}
			}
		}
		try {
			FileWrite.writeToFile(null, "Completed merging", "Tracker.txt",
					true);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return fingerpoints;
	}

	public List<FingerPrint> compareSFFPFingerPrint(
			List<FingerPrint> fingerpoints, List<SFFP> refPts) {
		int i = 0;
		for (FingerPrint fingerprint : fingerpoints) {
			fingerprint.setRealroomPos(refPts.get(i).getNearRoomNum());
			i++;
		}
		return fingerpoints;
	}

	public double getDistance(FingerPrint x, Cluster y) {
		double x2 = Math.pow((x.getX() - y.getX()), 2);
		double y2 = Math.pow(x.getY() - y.getY(), 2);
		return Math.sqrt(x2 + y2);
	}

	public double getDistance(FingerPrint x, SFFP y) {
		double x2 = Math.pow((x.getX() - y.getX()), 2);
		double y2 = Math.pow(x.getY() - y.getY(), 2);
		return Math.sqrt(x2 + y2);
	}
}
