package com.karthik;
import java.io.File;
import java.util.List;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;
import android.os.Environment;
import android.util.Log;

public class JAXB {
public void writeToXML(List<FingerPrint> fingerPoints,String xmlFileName) 
{
	FingerPrints fingerPrints = new FingerPrints();
	fingerPrints.setFingerpoints(fingerPoints);
	File root = Environment.getExternalStorageDirectory();
	File file = new File(root, xmlFileName);
	Serializer serial = new Persister();
	if(root.canWrite()){
		try {
			serial.write(fingerPrints, file);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}

public List<FingerPrint> getFingerPrint(String xmlFileName)
{
	Serializer serial = new Persister();
	File root = Environment.getExternalStorageDirectory();
	File file = new File(root, xmlFileName);
	if(root.canRead()){
		try {
			FingerPrints fingerPrints = serial.read(FingerPrints.class, file);
			if(fingerPrints !=null)
			Log.d("GetFingerPrint", "Not null");
			else
			Log.d("GetFingerPrint", "Not null");
			return fingerPrints.getFingerpoints();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	return null;
	
}
}
