package com.karthik;

import java.util.Collections;
import java.util.List;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;

public class FingerPrint {
	@ElementList
	private List<RSSI> fingerprints;
	@Element
	private int distance;
	@Element
	private int pos;
	@ElementList
	private List<Integer> fromPos;
	@Element
	private boolean isMerged;
	@Element
	private int mergedPos;
	@Element
	private double betweeness;
	@Element
	private boolean isCorridor;
	@Element
	private double X;
	@Element
	private double Y;
	@Element
	private int roomPos;
	@Element
	private int refptPos;
	@Element
	private int realroomPos;
	@Element
	private String locationName;

	public FingerPrint(int distance, List<RSSI> fingerprints, int pos,
			List<Integer> fromPos) {
		this.setPos(pos);
		this.setFromPos(fromPos);
		this.setDistance(distance);
		Collections.sort(fingerprints);//Check with the performance
		this.setFingerprints(fingerprints);
		this.setMerged(false);
		this.setMergedPos(-1);
	}
	public FingerPrint(){
		
	}

	public List<RSSI> getFingerprints() {
		return fingerprints;
	}

	public void setFingerprints(List<RSSI> fingerprints) {
		this.fingerprints = fingerprints;
	}

	public int getDistance() {
		return distance;
	}

	public void setDistance(int distance) {
		this.distance = distance;
	}

	public int getPos() {
		return pos;
	}

	public void setPos(int pos) {
		this.pos = pos;
	}

	public List<Integer> getFromPos() {
		return fromPos;
	}

	public void setFromPos(List<Integer> fromPos) {
		this.fromPos = fromPos;
	}

	public boolean isMerged() {
		return isMerged;
	}

	public void setMerged(boolean isMerged) {
		this.isMerged = isMerged;
	}

	public int getMergedPos() {
		return mergedPos;
	}

	public void setMergedPos(int mergedPos) {
		this.mergedPos = mergedPos;
	}

	public double getBetweeness() {
		return betweeness;
	}

	public void setBetweeness(double betweeness) {
		this.betweeness = betweeness;
	}

	public boolean isCorridor() {
		return isCorridor;
	}

	public void setCorridor(boolean isCorridor) {
		this.isCorridor = isCorridor;
	}

	public double getX() {
		return X;
	}

	public void setX(double x) {
		X = x;
	}

	public double getY() {
		return Y;
	}

	public void setY(double y) {
		Y = y;
	}

	public int getRoomPos() {
		return roomPos;
	}

	public void setRoomPos(int roomPos) {
		this.roomPos = roomPos;
	}

	public int getRealroomPos() {
		return realroomPos;
	}

	public void setRealroomPos(int realroomPos) {
		this.realroomPos = realroomPos;
	}

	public int getRefptPos() {
		return refptPos;
	}

	public void setRefptPos(int refptPos) {
		this.refptPos = refptPos;
	}

	public String getLocationName() {
		return locationName;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

}
