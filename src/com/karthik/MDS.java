package com.karthik;

import java.io.IOException;

import Jama.EigenvalueDecomposition;
import Jama.Matrix;

public class MDS {
	public double[][] CreateMDS(double d[][]) throws IOException {
		int m = d.length;
		int n = d[0].length;
		
		//FileWrite.writeToFile(d, "CreateMDS: " + m + " x " + n, "Matrix.txt",true);
		// double P2[][] = multiply(d, d);
		double P2[][] = new double[m][n];

		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				P2[i][j] = d[i][j] * d[i][j];
			}

		}
		double[][] I = identity(m);
		double[][] IN = allOne(m);
		double J[][] = subtract(I, IN);
		double temp[][] = multiply(J, P2);
		double BTemp[][] = multiply(temp, J);
		for (int i = 0; i < m; i++)
			for (int j = 0; j < n; j++)
				BTemp[i][j] = BTemp[i][j] * (-0.5);

		Matrix matrix = new Matrix(BTemp);
		EigenvalueDecomposition eig = new EigenvalueDecomposition(matrix);
		double[][] V = eig.getV().getArray();
		double[][] D = eig.getD().getArray();

		double max1 = -99999, max2 = -99999;
		int col1 = 0;
		int row1 = 0;
		int row2 = 0;
		int col2 = 0;
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				if (D[i][j] > max1) {
					max1 = D[i][j];
					col1 = j;
					row1 = i;
				}
			}
		}

		D[row1][col1] = -9999;
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				if (D[i][j] > max2) {
					max2 = D[i][j];
					col2 = j;
					row2 = i;
				}
			}
		}
		D[row1][col1] = max1;
		double EV[][] = new double[n][2];

		for (int i = 0; i < n; i++) {
			for (int j = 0; j < 2; j++) {
				int col = (j % 2 == 0 ? col1 : col2);
				EV[i][j] = V[i][col];
			}

		}

		double ED[][] = new double[2][2];

		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < 2; j++) {
				if (j % 2 == 0 && i % 2 == 0) {
					ED[i][j] = Math.sqrt(D[row1][col1]);
				} else if (j % 2 == 1 && i % 2 == 1) {
					ED[i][j] = Math.sqrt(D[row2][col2]);
				} else
					ED[i][j] = 0;
			}

		}

		return multiply(EV, ED);

	}

	public double[][] subtract(double[][] A, double[][] B) {
		int m = A.length;
		int n = A[0].length;
		double[][] C = new double[m][n];
		for (int i = 0; i < m; i++)
			for (int j = 0; j < n; j++)
				C[i][j] = A[i][j] - B[i][j];
		return C;
	}

	public double[][] identity(int n) {
		double[][] I = new double[n][n];
		for (int i = 0; i < n; i++)
			I[i][i] = 1;
		return I;
	}

	public double[][] allOne(int n) {
		double[][] I = new double[n][n];
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				I[i][j] = 1 / (double) n;
			}
		}

		return I;
	}

	public double[][] multiply(double[][] A, double[][] B) {
		int mA = A.length;
		int nA = A[0].length;
		int mB = B.length;
		int nB = A[0].length;
		if (nA != mB)
			throw new RuntimeException("Illegal matrix dimensions.");
		double[][] C = new double[mA][nB];
		for (int i = 0; i < mA; i++)
			for (int j = 0; j < nB; j++)
				for (int k = 0; k < nA; k++)
					C[i][j] += (A[i][k] * B[k][j]);
		return C;
	}
}
