package com.karthik;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


import android.os.Environment;
import android.util.Log;

public class FingerPrintSpace {
	
	MDS mds =new MDS();
	int UPPERBOUND = 10;
	public List<FingerPrint> findDifference(List<FingerPrint> rssiValues) throws IOException {

		List<FingerPrint> fingerpoints = new ArrayList<FingerPrint>();
		int cnt = rssiValues.size();
		try {
		for (int i = 0; i < cnt; i++) {
			
			FingerPrint rssiX = rssiValues.get(i);
			if (!rssiX.isMerged())
				for (int j = 0; j < cnt; j++) {
					
					if (i != j) {
						FingerPrint rssiY = rssiValues.get(j);
						if(!rssiY.isMerged())
						{
						if (rssiX.getFingerprints().size() != rssiY
								.getFingerprints().size()) {
							if (!fingerpoints.contains(rssiX))
								fingerpoints.add(rssiX);
						} else {
							if (getDiff(i, j,rssiValues) >= UPPERBOUND) {
								if (!fingerpoints.contains(rssiX))
									fingerpoints.add(rssiX);
							} 
							else {
								// Merge points
								
								for(FingerPrint temp:rssiValues)
								{
									if(temp.getFromPos().contains(rssiY.getPos()) && temp.getPos()!=0) {
										//temp.setDistance(temp.getDistance() + rssiY.getDistance());// (temp.getDistance() + rssiY.getDistance())need to chk to keep the same dis
										//temp.setFromPos(rssiY.getFromPos());
										
										for(int k=0; k < temp.getFromPos().size();k++)
										{
											if(temp.getFromPos().get(k) == rssiY.getPos())
											{
												temp.getFromPos().remove(k);
												break;
											}
										}
										temp.getFromPos().add(rssiX.getPos());
										//temp.setFromPos(rssiX.getPos());
										//merging =true;
										break;
									}
									
								}
								for(int fromPos:rssiY.getFromPos())
								{
									FingerPrint previous = rssiValues.get(fromPos);
									if(rssiX.getPos() !=previous.getPos())
										previous.getFromPos().add(rssiX.getPos());
								}
								
								
								
								/*if(!merging)
								{
									rssiX.setFromPos(rssiY.getFromPos());
									rssiX.setDistance(rssiY.getDistance());
								}*/
								
								rssiY.setMerged(true);
								rssiY.setMergedPos(rssiX.getPos());
							}
						}
					}
					}
				}
		}

		File root = Environment.getExternalStorageDirectory();
		File file = new File(root, "fingerpoints.txt");

		if (root.canWrite()) {
			FileWriter filewriter = new FileWriter(file, false);
			/*for (FingerPrint fingerprint : fingerpoints) {

				List<RSSI> rssi = fingerprint.getFingerprints();
				StringBuilder sb = new StringBuilder();
				for (int i = 0; i < rssi.size(); i++) {
					sb.append(rssi.get(i).getSSID() + ","
							+ rssi.get(i).getRssiLevel() + "\n");
				}
				sb.append("\n\n");
				filewriter.write(sb.toString());
			}*/
			for (FingerPrint fingerprint : rssiValues) {

				StringBuilder sb = new StringBuilder();
				sb.append("# " + fingerprint.getPos() + " From: "
						+ fingerprint.getFromPos() + " Distance: "
						+ fingerprint.getDistance() + "Merged " + fingerprint.isMerged() +
						"Merged with" + fingerprint.getMergedPos() + 
						"\n");
				
				filewriter.write(sb.toString());
			}
			filewriter.close();
		}
		File file1 = new File(root, "fingerpointsdistance.txt");
		if (root.canWrite()) {
			FileWriter filewriter = new FileWriter(file1, false);
			for (FingerPrint fingerprint : fingerpoints) {
				StringBuilder sb = new StringBuilder();
				sb.append("# " + fingerprint.getPos() + " From: "
							+ fingerprint.getFromPos() + " Distance: "
							+ fingerprint.getDistance() + "\n");
				filewriter.write(sb.toString());
			}

			filewriter.close();
		}
		}
		catch(Exception e)
		{
			Log.d("findDifference", e.toString());
		}
		return fingerpoints;
	}


	public double[][] createFingerPrintSpace(List<FingerPrint> fingerpoints) throws IOException {
		
		LoadGraph loadGraph = new LoadGraph();
		Graph graph= loadGraph.populateGraph(fingerpoints);
		FloydWarshallAPSP floydWarshall = new FloydWarshallAPSP();
		double d[][] = floydWarshall.getShortestPath(graph);
		FileWrite.writeToFile(d, "", "Shortestpath.txt", false);
		
	//	int size = fingerpoints.size();
	//	double d[][] = new double[size][size];
		
		try {
			/*for (int x = 0; x < size; x++) {
				//int locationx = fingerpoints.get(x).getDistance();
				FingerPrint fingerPrintX = fingerpoints.get(x);
				for (int y = 0; y < size; y++) {
					FingerPrint fingerPrintY = fingerpoints.get(y);
					if(x==y)
					{
						d[x][y] =0;
					}
					else if(x<y)
					{
						d[x][y]=returnDistance(fingerPrintX,fingerPrintY,fingerpoints);
					}
					else if(x>y)
					{
							d[x][y] =d[y][x];
					}
					//d[x][y] = (x == y ? 0 : Math.abs(fingerPrintY.getDistance() - fingerPrintX.getDistance()));
				}
			}*/

			
	
		} catch (Exception e) {
			
			FileWrite.writeToFile(d, e.toString() + " Size RSSI Values:" + d.length, "FingerPrintSpaceMDS.txt",false);
		}
		return d;
	}
	
	/*public int returnDistance(FingerPrint x,FingerPrint y,List<FingerPrint> fingerpoints)
	{
		int sum =0;
		while(x.getPos() != y.getFromPos())
		{
			sum = sum + y.getDistance();
			y = searchFingerPrint(y.getFromPos(),fingerpoints);
			if(y==null)
				break;
		}
		if(y!=null)
		sum = sum + y.getDistance();
		return sum;
	}*/
	
	public FingerPrint searchFingerPrint(int pos,List<FingerPrint> fingerpoints)
	{
		for(FingerPrint y:fingerpoints)
		{
			if(y.getPos()==pos)
				return y;
		}
		return null;
	}

	public int getDiff(int s, int t,List<FingerPrint> rssiValues) {

		List<RSSI> rssiX = rssiValues.get(s).getFingerprints();
		List<RSSI> rssiY = rssiValues.get(t).getFingerprints();
		int cnt = rssiX.size() <= rssiY.size() ? rssiX.size() : rssiY.size();
		int sum = 0;
		for (int i = 0; i < cnt; i++) {
			sum = sum
					+ Math.abs((rssiX.get(i).getRssiLevel() - rssiY.get(i)
							.getRssiLevel()));
		}
		return sum;
	}
	public double getDiff(List<RSSI> rssiX,List<RSSI> rssiY) {
		double sum=0;
		int cnt = rssiX.size() <= rssiY.size() ? rssiX.size() : rssiY.size();
		//if(rssiX.size()!=rssiY.size())
			//return Double.POSITIVE_INFINITY;
		for(int i=0;i<cnt;i++){
			RSSI x = rssiX.get(i);
			RSSI y = rssiY.get(i);
			sum+= Math.pow((x.getRssiLevel() - y.getRssiLevel()), 2);
		}
		
		return Math.sqrt(sum);
		
		/*int cnt = rssiX.size() <= rssiY.size() ? rssiX.size() : rssiY.size();
		int sum = 0;
		for (int i = 0; i < cnt; i++) {
			sum = sum
					+ Math.abs((rssiX.get(i).getRssiLevel() - rssiY.get(i)
							.getRssiLevel()));
		}
		return sum;*/
		
		
		
	}

}
