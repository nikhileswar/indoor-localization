package com.karthik;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Vector;

public class Graph {
	@SuppressWarnings("rawtypes")
	protected Vector<Vertex> nodes = new Vector<Vertex>();
	protected Vector<Edge> edges = new Vector<Edge>();
	protected boolean sortedNeighbors = false;

	public double[][] getAdjacencyMatrix() throws IOException {
		double[][] adj = new double[nodes.size()][nodes.size()];
	
		// Initialize the adjacency matrix and assign the nodes neighbor to it
		for (int i = 0; i < nodes.size(); i++) {
			for (int j = 0; j < nodes.size(); j++) {
				if (i == j)
					adj[i][j] = 0;
				else
					adj[i][j] = Double.POSITIVE_INFINITY;

			}
		}

		// Find the neighbor
		for (int i = 0; i < nodes.size(); i++) {
			@SuppressWarnings("rawtypes")
			Vertex vertex = nodes.elementAt(i);
			for (int j = 0; j < edges.size(); j++) {
				Edge edge = edges.elementAt(j);
				if (edge.a == vertex) {
					adj[i][nodes.indexOf(edge.b)] = edge.edgeweight;
				}
				else if (edge.b == vertex) {
					adj[nodes.indexOf(edge.b)][j] = edge.edgeweight;
				}
			}
		}
		return adj;

	}

	public boolean isSortedNeighbors() {
		return sortedNeighbors;
	}

	public void setSortedNeighbors(boolean flag) {
		sortedNeighbors = flag;
	}

	@SuppressWarnings("rawtypes")
	public int indexOf(Vertex a) {
		for (int i = 0; i < nodes.size(); i++)
			if (nodes.elementAt(i).data.equals(a.data))
				return i;

		return -1;
	}

	@SuppressWarnings("rawtypes")
	public Vector<Vertex> getNodes() {
		return nodes;
	}

	public Vector<Edge> getEdges() {
		return edges;
	}

	@SuppressWarnings("rawtypes")
	public Vertex getNodeAt(int i) {
		return nodes.elementAt(i);
	}

	@SuppressWarnings("rawtypes")
	public int addNode(Vertex a) {
		nodes.add(a);
		return nodes.size() - 1;
	}

	public void addEdge(Edge a) {
		edges.add(a);

	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Vector<Vertex> getNeighbors(Vertex a) {
		Vector<Vertex> neighborvertex = new Vector<Vertex>();

		for (int i = 0; i < edges.size(); i++) {
			Edge edge = edges.elementAt(i);
			if (edge.a == a)
				neighborvertex.add(edge.b);
		}
		if (sortedNeighbors)
			Collections.sort(neighborvertex);
		return neighborvertex;
	}
	@SuppressWarnings({ "rawtypes" })
	public Vector<Edge> getNeighborsEdges(Vertex a) {
		Vector<Edge> neighborEdge = new Vector<Edge>();
		for (int i = 0; i < edges.size(); i++) {
			Edge edge = edges.elementAt(i);
			if (edge.a == a)
				neighborEdge.add(edge);
		}
			return neighborEdge;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List<Edge> getNeighborsEdge(Vertex a) {
		return a.adj;
		/*Vector<Edge> neighborEdge = new Vector<Edge>();
		for (int i = 0; i < edges.size(); i++) {
			Edge edge = edges.elementAt(i);
			if (edge.a == a)
				neighborEdge.add(edge);
		}
			return neighborEdge;*/
	}

}
