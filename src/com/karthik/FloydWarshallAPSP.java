package com.karthik;

import java.io.IOException;



public class FloydWarshallAPSP {

	public FloydWarshallAPSP() {

	}

	public double[][] getShortestPath(Graph graph) throws IOException {
		double adj[][] = graph.getAdjacencyMatrix();
		for (int k = 0; k < adj.length; k++) {
			for (int i = 0; i < adj.length; i++) {
				for (int j = 0; j < adj.length; j++) {
					/*if(i >  j)
						adj[i][j] = adj[j][i];
					else*/
						adj[i][j] = Math.min(adj[i][j], (adj[i][k] + adj[k][j]));
				}
			}
		}
		/*PrintWriter out;
		out = new PrintWriter(FloydWarshallAPSP.class.getName() + ".txt");
		for (int i = 0; i < adj.length; i++) {
			out.println("From " + graph.getNodeAt(i) + ":");
			for (int j = 0; j < adj.length; j++) {
				out.println("\tShortest Path distance to " + graph.getNodeAt(j)
						+ " is: " + adj[i][j]);
			}
			out.println();
		}

		out.close();*/
		
		return adj;
	}

	/*public static void main(String args[]) throws FileNotFoundException {
		if (args.length != 1 || args[0] == null) {
			System.out.println("Argument should not null");
			System.exit(0);
		}
		long t1, t2;
		LoadGraph loadGraph = new LoadGraph(args[0], false);
		FloydWarshallAPSP floydWarshall = new FloydWarshallAPSP();
		t1 = System.currentTimeMillis();
		Graph graph = loadGraph.populateGraph();
		t2 = System.currentTimeMillis();
		System.out.println("Time taken to populate graph: " + (t2 - t1) + "ms");
		t1 = System.currentTimeMillis();
		floydWarshall.getShortestPath(graph);
		t2 = System.currentTimeMillis();
		System.out.println("Time taken to find shortest using FloydWarshallAPSP: "
				+ (t2 - t1) + "ms");

	}*/
}
