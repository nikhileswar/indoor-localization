package com.karthik;

public class Edge {
	@SuppressWarnings("rawtypes")
	protected Vertex a, b;
    protected double edgeweight;
     
    @SuppressWarnings("rawtypes")
	public Edge(Vertex a, Vertex b) {
        this(a, b, Double.POSITIVE_INFINITY);
    }
     
    @SuppressWarnings("rawtypes")
	public Edge(Vertex a, Vertex b, double edgeweight) {
        this.a = a;
        this.b = b;
        this.edgeweight = edgeweight;
    }
     
    public double getEdgeWeight() {
        return edgeweight;
    }
     
    public String toString() {
        return a + " --> " + b;
    }
 
}
