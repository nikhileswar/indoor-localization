package com.karthik;

import java.util.List;

public class StepCount {
	 
	 double B11 = 2;
	 double B21 = 1;
	 int SAMPLECOUNT = 15;
	 
	public int calculateStepCount(int i,List<Double> sample) {

		double B1[] = new double[sample.size()];
		double B2[] = new double[sample.size()];
		double meanSum = 0.0;
		double mean[] = new double[i];
		double varianceSum = 0;
		double variance[] = new double[i];
		if (i < SAMPLECOUNT)
			SAMPLECOUNT = i;
		else
			SAMPLECOUNT = 15;
		for (int j = 0; j < i; j++) {
			for (int k = j - SAMPLECOUNT; k < j + SAMPLECOUNT; k++) {
				if (k >= 0 && k < i)
					meanSum += sample.get(k);
			}
			mean[j] = meanSum / (2 * SAMPLECOUNT + 1);
			meanSum = 0;
		}

		for (int j = 0; j < i; j++) {
			for (int k = j - SAMPLECOUNT; k < j + SAMPLECOUNT; k++) {
				if (k >= 0 && k < i)
					varianceSum += Math.pow((sample.get(j) - mean[j]), 2);
			}

			variance[j] = varianceSum / (2 * SAMPLECOUNT + 1);
			varianceSum = 0;

		}

		for (int j = 0; j < i; j++) {
			if (Math.sqrt(variance[j]) > B11) {
				B1[j] = B11;
			} else
				B1[j] = 0;
		}

		for (int j = 0; j < i; j++) {
			if (Math.sqrt(variance[j]) < B21) {
				B2[j] = B21;
			} else
				B2[j] = 0;
		}

		// Calculating number of steps

		int stepCount = 0;

		for (int j = 0; j < i - 1; j++) {
			if (B1[j] < B1[j + 1])
			{
				//if(chkLowAcceleration(SAMPLECOUNT,j,B2))
					stepCount++;
			}
				
		}
		return stepCount;
	}
	
	public boolean chkLowAcceleration(int sampleCount,int pos,double B2[])
	{
		int end = ((pos + sampleCount- 1)<B2.length?(pos + sampleCount- 1):B2.length);
		for (int j = pos; j < end; j++) {
			if(B2[j] == B21)
				return true;
		}
		return false;
	}
}
