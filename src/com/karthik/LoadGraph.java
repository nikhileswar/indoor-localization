package com.karthik;


import java.io.IOException;
import java.util.List;

public class LoadGraph {

	@SuppressWarnings("unchecked")
	public Graph populateGraph(List<FingerPrint> fingerpoints)
			throws IOException {

		Graph graph = new Graph();
		int m = fingerpoints.size();
		//int n = d[0].length;
		/*for (int i = 0; i < m; i++) {
			FingerPrint rssiX = fingerpoints.get(i);
			for (int j = 0; j < m ; j++) {
				FingerPrint rssiY = fingerpoints.get(j);
				if (i != j) {
					Vertex<Integer> a = new Vertex<Integer>(fingerpoints.get(i)
							.getPos());
					Vertex<Integer> b = new Vertex<Integer>(fingerpoints.get(j)
							.getPos());
					int aPos = graph.indexOf(a);
					int bPos = graph.indexOf(b);
					if (aPos == -1)
						aPos = graph.addNode(a);
					if (bPos == -1)
						bPos = graph.addNode(b);
					double edgeWeight = d[i][j];

					Edge edge = new Edge(graph.getNodeAt(aPos),
							graph.getNodeAt(bPos), edgeWeight);
					graph.getNodeAt(aPos).adj.add(edge);
					graph.addEdge(edge);
				}
			}
		}*/
		
		for (int i = 0; i < m; i++) {
			FingerPrint rssiX = fingerpoints.get(i);
			if(rssiX.getPos()!=0)
			{
				for(int fromPos: rssiX.getFromPos())
				{
					Vertex<Integer> a = new Vertex<Integer>(fromPos);
					Vertex<Integer> b = new Vertex<Integer>(rssiX.getPos());
					
					int aPos = graph.indexOf(a);
					int bPos = graph.indexOf(b);
					if (aPos == -1)
						aPos = graph.addNode(a);
					if (bPos == -1)
						bPos = graph.addNode(b);
					double edgeWeight = rssiX.getDistance();

					Edge edge = new Edge(graph.getNodeAt(aPos),
							graph.getNodeAt(bPos), edgeWeight);
					graph.getNodeAt(aPos).adj.add(edge);
					graph.addEdge(edge);
				}
				
			}
		}

		return graph;
	}
}
