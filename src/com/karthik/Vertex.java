package com.karthik;

import java.util.LinkedList;
import java.util.List;

public class Vertex<T> implements Comparable<Vertex<T>> {
	protected T data;
	public double distance = Double.POSITIVE_INFINITY;
	public Vertex<T> predecessor = null;
	public List<Edge> adj;

	public Vertex() {
		
	}

	public Vertex(T data) {
		this.data = data;
		adj = new LinkedList<Edge>();
	}

	public String toString() {
		return data.toString();
	}

	@Override
	public int compareTo(Vertex<T> o) {
		String tempA = this.toString();
		String tempB = o.toString();
		return tempA.compareTo(tempB);
	}

}
