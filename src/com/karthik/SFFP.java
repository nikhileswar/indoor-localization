package com.karthik;

public class SFFP {
private double X;
private double Y;
private int roomNum;
private String locationName;
private int nearRoomNum;
public double getX() {
	return X;
}
public void setX(double x) {
	X = x;
}
public double getY() {
	return Y;
}
public void setY(double y) {
	Y = y;
}
public int getRoomNum() {
	return roomNum;
}
public void setRoomNum(int roomNum) {
	this.roomNum = roomNum;
}
public String getLocationName() {
	return locationName;
}
public void setLocationName(String locationName) {
	this.locationName = locationName;
}
public int getNearRoomNum() {
	return nearRoomNum;
}
public void setNearRoomNum(int nearRoomNum) {
	this.nearRoomNum = nearRoomNum;
}

}
