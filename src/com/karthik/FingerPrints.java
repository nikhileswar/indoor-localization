package com.karthik;

import java.util.List;


import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;


@Root
public class FingerPrints {
@ElementList
private List<FingerPrint> fingerpoints;

public List<FingerPrint> getFingerpoints() {
	return fingerpoints;
}

public void setFingerpoints(List<FingerPrint> fingerpoints) {
	this.fingerpoints = fingerpoints;
}

}
