package com.karthik;

public class Cluster {
private double x;
private double y;
private int roomPos;
public double getX() {
	return x;
}
public void setX(double x) {
	this.x = x;
}
public double getY() {
	return y;
}
public void setY(double y) {
	this.y = y;
}
public int getRoomPos() {
	return roomPos;
}
public void setRoomPos(int roomPos) {
	this.roomPos = roomPos;
}
}
